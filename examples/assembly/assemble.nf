nextflow.enable.dsl = 2

params.outdir = "${baseDir}/out"
params.kmerlen = 71
params.cov_cutoff = 0
params.with_velvet = null
params.with_spades = null

process runVelveth {
    publishDir params.outdir, mode: 'copy', overwrite: true
    container "https://depot.galaxyproject.org/singularity/velvet%3A1.2.10--h7132678_5"
    input:
        path fastq
    output:
        path "hash"
    script:
    if(fastq instanceof List) {
        """
        velveth hash ${params.kmerlen} -shortPaired -fastq ${fastq[0]} ${fastq[1]}
        """
    }
    else {
        """
        velveth hash ${params.kmerlen} -fastq ${fastq}
        """
    }
}

process runVelvetg {
    publishDir params.outdir, mode: 'copy', overwrite: true
    container "https://depot.galaxyproject.org/singularity/velvet%3A1.2.10--h7132678_5"
    input:
        path hashdir
    output:
        path "${hashdir}/contigs.fa"
        path "${hashdir}"
    """
    velvetg ${hashdir} -cov_cutoff ${params.cov_cutoff}
    """
}

process runSpades {
    container "https://depot.galaxyproject.org/singularity/spades%3A3.15.5--h95f258a_0"
    publishDir params.outdir, mode: 'copy', overwrite: true
    input:
        path fastqfile
    output:
        path "spades_out", emit: fulldirectory
        path "spades_out/contigs.fasta", emit: contigs
    """
    spades.py -s ${fastqfile} -o spades_out
    """
}

workflow {
    fastqin_channel = Channel.fromPath("${params.indir}/*.fastq").collect()
    if(params.with_velvet != null) {
        velveth_channel = runVelveth(fastqin_channel)
        runVelvetg(velveth_channel)
    }
    if(params.with_spades != null) {
        runSpades(fastqin_channel)
    }
}