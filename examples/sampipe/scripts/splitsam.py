import sys

samfilename = sys.argv[1]
prefix = sys.argv[2]

print("Reading SAM file " + samfilename + "...")

# This counts the number of sequences read from the sam file
numsequence = 0
with open(samfilename, "r") as samfile:
    for line in samfile.readlines():
        if line[0] == "@":
            continue
        data = line.split("\t")
        sequence_name = data[0]
        sequence_data = data[9]
        # Generate the filename of the FASTA file from the prefix and the sequence number
        # Alternatively for instance: hex(numsequence)
        # Or to go to characters via ASCII table: chr(65+numsequence)
        outfilename = prefix + str(numsequence) + ".fasta"
        print("  Writing sequence to " + outfilename + "...")
        with open(outfilename, "w") as outfile:
            outfile.write(">" + sequence_name + "\n")
            outfile.write(sequence_data + "\n")
        numsequence += 1

print("Done.")
