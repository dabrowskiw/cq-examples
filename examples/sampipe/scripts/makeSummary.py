import sys

#["start1", "start2", "start3", "stop1", "stop2", "stop3"]

print(sys.argv)
numfiles = int((len(sys.argv)-1)/2)
print("Summarizing files for " + str(numfiles) + " sequences")

outfile = open("summary.csv", "w")
outfile.write("# Sequence number, number of start codons, number of stop codons\n")

for numfile in range(0, numfiles):
    filename = sys.argv[numfile+1]
    with open(filename, "r") as startfile:
        numstarts = startfile.read().strip()
    stopfilename = sys.argv[numfiles+numfile+1]
    with open(stopfilename, "r") as stopfile:
        numstops = stopfile.read().strip()
    sequencename = filename.split("_")[1].split(".")[0]
    outfile.write(sequencename + ", " + numstarts + ", " + numstops + "\n")

outfile.close()