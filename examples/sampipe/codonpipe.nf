nextflow.enable.dsl = 2

params.outdir = "${baseDir}/output"
params.cachedir = "${baseDir}/cache"
params.url = null

process downloadSam {
    storeDir params.cachedir
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        val url
    output:
        path "newfile.sam"
    """
    wget ${url} -O newfile.sam
    """
}

process splitSam {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path samfile
    output:
        path "sequence_*.fasta"
    """
    python ${baseDir}/scripts/splitsam.py ${samfile} sequence_
    """
}

process countStarts {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastafile
    output:
        path "${fastafile}.numstarts"
    """
    tail -1 ${fastafile} | grep -o "ATG" | wc -l > ${fastafile}.numstarts
    """
}

process countStops {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastafile
    output:
        path "${fastafile}.numstops"
    """
    tail -1 ${fastafile} | grep -o "TAA" | wc -l > ${fastafile}.numstops
    """
}

process makeSummary {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path numstartfiles
        path numstopfiles
    output:
        path "summary.csv"
    """
#    python ${baseDir}/scripts/makeSummary.py ${numstartfiles}
    python ${baseDir}/scripts/makeSummary.py *.numstarts *.numstops
    """
}

workflow {
    if(params.url == null) {
        print "Please provide SAM file download URL via --url"
        System.exit(0)
    }
    samfiles = downloadSam(Channel.from(params.url))
    splitfiles = splitSam(samfiles).flatten()
    startfiles = countStarts(splitfiles).collect()
    stopfiles = countStops(splitfiles).collect()
    makeSummary(startfiles, stopfiles)
// Method 1:
//    startfiles = countStarts(splitfiles)
//    stopfiles = countStops(splitfiles)
//    allfiles = startfiles.concat(stopfiles).collect()
//    makeSummary(allfiles)
}