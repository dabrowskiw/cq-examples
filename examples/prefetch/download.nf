nextflow.enable.dsl = 2

params.outdir = "${baseDir}/out"
params.storedir = "${baseDir}/cache"
params.with_stats = null
params.with_fastqc = null
params.with_fastp = null
params.multiqc_logo = "${baseDir}/logo.png"

process prefetch {
    container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5321ha49a11a_3"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    storeDir params.storedir
    input:
        val accession
    output:
        path "${accession}.sra"
    """
    prefetch ${accession}
    mv ${accession}/${accession}.sra .
    """
}

process dump {
    container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5321ha49a11a_3"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    storeDir params.storedir
    input:
        path srafile
    output:
        path "${srafile.getSimpleName()}*.fastq"
    """
    fastq-dump --split-3 ${srafile}
    """
}

process makeFastqStats {
    container "https://depot.galaxyproject.org/singularity/ngsutils%3A0.5.9--py27h9801fc8_5"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastqfile
    output:
        path "${fastqfile.getSimpleName()}.stats"
    """
    fastqutils stats ${fastqfile} > ${fastqfile.getSimpleName()}.stats
    """
}

process runFastqc {
    container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.5--hdfd78af_5"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastqfile
    output:
        path "${fastqfile.getSimpleName()}_fastqc.html", emit: html
        path "${fastqfile.getSimpleName()}_fastqc.zip", emit: zip
    """
    fastqc ${fastqfile}
    """
}

process runFastp {
    container "https://depot.galaxyproject.org/singularity/fastp%3A0.23.2--hb7a2d85_2"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastqfile
    output:
        path "${fastqfile.getSimpleName()}trimmed.fastq", emit: seqfile
        path "${fastqfile.getSimpleName()}_fastp.html", emit: report_html
        path "${fastqfile.getSimpleName()}_fastp.json", emit: report_json
    """
    fastp -i ${fastqfile} -o ${fastqfile.getSimpleName()}trimmed.fastq
    mv fastp.html ${fastqfile.getSimpleName()}_fastp.html
    mv fastp.json ${fastqfile.getSimpleName()}_fastp.json
    """
}

process runMultiQC {
    container "https://depot.galaxyproject.org/singularity/multiqc%3A1.13a--pyhdfd78af_1"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input: 
        path infiles
    output:
        path "multiqc_report.html"
        path "multiqc_data"
    """
    multiqc .
    """
}

process replaceLogoPath {
    input:
        path configfile
    output:
        path "multiqc_config.yaml"
    """
    awk '{gsub("LOGOPATH", "${params.multiqc_logo}", \$0); print}' multiqc_config.template > multiqc_config.yaml
    """
}

workflow {
    srachannel = prefetch(params.accession)
    fastqchannel = dump(srachannel).flatten()
    multiqcinchannel = replaceLogoPath(Channel.fromPath("${baseDir}/multiqc_config.template"))
    if(params.with_stats != null) {
        makeFastqStats(fastqchannel)
    }
    if(params.with_fastp != null) {
        trimmedfastqchannel = runFastp(fastqchannel)
        allfastqchannel = fastqchannel.concat(trimmedfastqchannel.seqfile)
        multiqcinchannel = trimmedfastqchannel.report_json.concat(multiqcinchannel)
    } 
    else {
        allfastqchannel = fastqchannel
    }
    if(params.with_fastqc != null) {
        fastqcoutchannel = runFastqc(allfastqchannel)
        multiqcinchannel = fastqcoutchannel.zip.concat(multiqcinchannel)
    }

    runMultiQC(multiqcinchannel.collect())
}

