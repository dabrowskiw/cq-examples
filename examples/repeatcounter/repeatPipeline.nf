nextflow.enable.dsl = 2

params.outdir = baseDir
params.tempdir = "${baseDir}/cache"
params.downloadurl = null
params.infile = null

// This is a process for downloading a file
process downloadFile {
    storeDir "${params.tempdir}"
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        val inurl
    output:
        path "batch1.fasta"
    """
    wget ${inurl} -O batch1.fasta
    """    
}

// Count the number of sequences in a FASTA File
process countSequences {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path infile
    output:
        path "numseqs.txt"
    """
    grep ">" ${infile} | wc -l > numseqs.txt
    """
}

process splitSequences {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path infile
    output:
        path "sequence_*.fasta"
    """
    split -l 2 --additional-suffix .fasta -d ${infile} sequence_
    """
}

process countBases {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastafile
    output:
        path "${fastafile.getSimpleName()}_numbases.txt"
    """
    tail -1 ${fastafile} | wc -m > ${fastafile.getSimpleName()}_numbases.txt
    """
}

process countRepeats {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path fastafile
    output:
        path "${fastafile.getSimpleName()}_numreps.txt"
    """
    grep -o "GCCGCG" ${fastafile} | wc -l > ${fastafile.getSimpleName()}_numreps.txt
    """
}

process collectResults {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path textfiles
    output:
        path "summary.csv"
    """
    python ${baseDir}/collectResults.py *.txt
    """
}

workflow {
    if(params.downloadurl != null && params.infile == null) {
        fastafile = downloadFile(Channel.from(params.downloadurl))
    } else if(params.infile != null && params.downloadurl == null) {
        fastafile = Channel.fromPath(params.infile)
    } else {
        print "Please provide --infile or --downloadurl"
        System.exit(0)
    }
    countSequences(fastafile)
    splitfastas = splitSequences(fastafile)
    splitfastas_flat = splitfastas.flatten()
    countBases(splitfastas_flat)
    repeatcounts = countRepeats(splitfastas_flat)
    repeatcounts_collected = repeatcounts.collect()
    collectResults(repeatcounts_collected)
}